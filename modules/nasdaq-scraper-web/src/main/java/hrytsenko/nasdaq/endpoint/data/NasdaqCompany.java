package hrytsenko.nasdaq.endpoint.data;

import hrytsenko.nasdaq.domain.data.Company;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NasdaqCompany {

    private final String symbol;
    private final String name;

    private final String sector;
    private final String industry;

    @JsonCreator
    public NasdaqCompany(@JsonProperty("Symbol") String symbol, @JsonProperty("Name") String name,
            @JsonProperty("Sector") String sector, @JsonProperty("Industry") String industry) {
        this.symbol = symbol;
        this.name = name;
        this.sector = sector;
        this.industry = industry;
    }

    public static Company toCompany(NasdaqCompany nasdaqCompany) {
        Company company = new Company();

        company.setSymbol(nasdaqCompany.getSymbol());
        company.setName(nasdaqCompany.getName());

        company.setSector(nasdaqCompany.getSector());
        company.setSubsector(nasdaqCompany.getIndustry());

        return company;

    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public String getSector() {
        return sector;
    }

    public String getIndustry() {
        return industry;
    }

}
