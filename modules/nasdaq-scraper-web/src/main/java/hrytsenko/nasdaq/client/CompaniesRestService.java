package hrytsenko.nasdaq.client;

import hrytsenko.nasdaq.client.data.ClientCompany;
import hrytsenko.nasdaq.domain.CompaniesService;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.common.base.Strings;

@Path("companies")
@Stateless
public class CompaniesRestService {

    @Inject
    private CompaniesService companiesService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ClientCompany> findCompanies(@QueryParam("exchange") String exchange,
            @QueryParam("symbol") String symbol, @QueryParam("sector") String sector) {
        return companiesService
                .findCompanies(Strings.emptyToNull(exchange), Strings.emptyToNull(symbol), Strings.emptyToNull(sector))
                .stream().map(ClientCompany::fromCompany).collect(Collectors.toList());
    }

    @GET
    @Path("/sectors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> findSectors() {
        return companiesService.findSectors();
    }

}
