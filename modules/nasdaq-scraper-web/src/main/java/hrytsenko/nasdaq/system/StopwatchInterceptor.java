package hrytsenko.nasdaq.system;

import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import com.google.common.base.Stopwatch;

public class StopwatchInterceptor {

    private static final Logger LOGGER = Logger.getLogger(StopwatchInterceptor.class.getName());

    @AroundInvoke
    public Object log(InvocationContext context) throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            return context.proceed();
        } finally {
            LOGGER.info(() -> String.format("Invocation of %s::%s completed in %s.",
                    context.getTarget().getClass().getSimpleName(), context.getMethod().getName(), stopwatch));
        }
    }

}
